<?php

namespace App\Http\Controllers\Buyer;

use App\Buyer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class BuyerCategoryController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Buyer $buyer)
    {
        //Como las relaciones entre productos y categorias es de muchos a muchos, lo que nos devolverá será una coleccion de productos
        //donde cada uno tiene a su vez una coleccion de categorias, es decir una coleccion de coleccion.
        //Para juntar todas las colecciones en una sola, usamos colpase() y luego usar unique y values para filtrar los repetidos y
        //eliminar los indices vacios luego de esa limpieza con values()
        $categories = $buyer->transactions()
            ->with('product.categories')
            ->get()
            ->pluck('product.categories')
            ->collapse()
            ->unique('id')
            ->values();
            
        return $this->showAll($categories);
    }
}
