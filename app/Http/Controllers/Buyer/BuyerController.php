<?php

namespace App\Http\Controllers\Buyer;

use App\Http\Controllers\ApiController;
use App\Buyer;


class BuyerController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Al utilizar global scopes, all() trae filtrados los users que tienen transactions relacionadas
        $buyers = Buyer::all();
        return $this->showAll($buyers);
    }

    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Buyer $buyer)
    {
        //Inyección implicita de buyer
        return $this->showOne($buyer);
    }
}
