<?php

namespace App\Http\Controllers\Buyer;

use App\Buyer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class BuyerProductController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Buyer $buyer)
    {
        //Si accedemos a $buyer->transactions vamos a obtener una colección y no una instancia de transaction, por lo tanto no tendriamos una manera directa
        //de acceder al producto, $products = $buyer->transactions->product; esto daria error ya que la propiedad product no existe para la clase Collection.
        //Para lograr esto usaremos "eager loading" (https://laraveles.com/eager-loading-laravel/)
        //Accedemos al query builder de transactions usando los parentesis $buyer->transactions(), llamando a la función no a la relación, podemos seguir 
        //agregando restricciones tipo where, find, etc. En este caso usartemos with(), que recibe relaciones, en nuestro caso usaremos la relación product
        //y lo obtenemos con el método get(), este metodo nos devuelve las transacciones con los productos en su interior, pero nosotros sólo queremos obtener
        //los productos, para ellos utilizamos el método pluck() al cual le indicamos que queremos trabajar sólo con una parte de esa colección.

        $products = $buyer->transactions()->with('product')
            ->get()
            ->pluck('product')
            ->unique('id')
            ->values();

        return $this->showAll($products);
    }
}
