<?php

namespace App\Http\Controllers\Buyer;

use App\Buyer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class BuyerSellerController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Buyer $buyer)
    {
        //Recordemos que tenemos Buyer:1->N:Transaction:N->1:Product:N->1:Seller
        //Para llegar a los vendedores de cada comprador, debemos primero obtener las transacciones, sus productos
        //y por cada producto obtener a su vendedor, puede ocurrir que el comprador haya comprado siemper al mismo vendedor
        //por lo que tendremos instancias repetidas del mismo vendedor, y sólo debemos devolver una de ellas, lo haremos con
        //unique. No podemos usar directamente pluck('seller') porque está dentro de product.
        //Unique conserva el valor de los indices originales de la collection, es decir que si hay un vendedor que se repite, 
        //Unique dejará un valor vacío dentro de la colección, para evitar esto utilizaremos el metodo values() que reorganizará
        //los indices eliminando aquellos vacíos

        $sellers = $buyer->transactions()
            ->with('product.seller')
            ->get()
            ->pluck('product.seller')
            ->unique('id')
            ->values();

        return $this->showAll($sellers);
    }
}
