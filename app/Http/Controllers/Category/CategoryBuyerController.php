<?php

namespace App\Http\Controllers\Category;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class CategoryBuyerController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Category $category)
    {
        //Puede ser que el producto no tenga transacciones, lo resolvemos con whereHas
        //Puede pasar que varias transacciones del mismo producto sean del mismo buyer
        //Puede haber comprado el mismo buyer la misma categoria varias veces
        //obtenemos una colección de transacciones por lo cual no podemos usar directamtente transactions.buyer en el pluck()
        //antes usamos collpase, y al obtener una sola coleccion volver a usar pluck pero ahora con 'buyer'
        $buyers = $category->products()
            ->whereHas('transactions')
            ->with('transactions.buyer')
            ->get()
            ->pluck('transactions')
            ->collapse()
            ->pluck('buyer')
            ->unique('id')
            ->values();

        return $this->showAll($buyers);
    }
}
