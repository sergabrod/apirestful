<?php

namespace App\Http\Controllers\Category;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class CategoryTransactionController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Category $category)
    {
        //Aca puede pasar que un producto no tenga aun transacciones realizadas, debemos controlar esto, para eso usamos
        //el método whereHas()
        //Como un producto puede tener multiples transacciones usamos el metodo collapse para unir todo en una sola lista
        $transactions = $category->products()
            ->whereHas('transactions')
            ->with('transactions') 
            ->get()
            ->pluck('transactions')
            ->collapse();
        
        return $this->showAll($transactions);
    }
}
