<?php

namespace App\Http\Controllers\Product;

use App\Product;
use App\User;
use App\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\DB;

class ProductBuyerTransactionController extends ApiController
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Product $product, User $buyer)
    {
        $rules = [
            'quantity' => 'required|integer|min:1',
        ];

        $this->validate($request, $rules);

        //Debemos verificar que el buyer y seller sean diferentes usuarios
        if ($buyer->id == $product->seller_id) {
            return $this->errorResponse('El comprador y el vendedor deben ser usuarios diferentes', 409);
        }

        //El comprador debe ser un usuario verificado
        if (!$buyer->esVerificado()) {
            return $this->errorResponse('El comprador debe ser un usuario verificado', 409);
        }

        //El vendedor debe ser un usuario verificado
        if (!$product->seller->esVerificado()) {
            return $this->errorResponse('El vendedor debe ser un usuario verificado', 409);
        }

        //El producto debe estar disponible
        if (!$product->estaDisponible()) {
            return $this->errorResponse('El producto no está disponible para la venta', 409);
        }

        //La cantidad que se desea comprar no debe ser superior a la disponible
        if ($product->quantity < $request->quantity) {
            return $this->errorResponse('El producto no tiene la cantidad requerida por la transaccion', 409);
        }

        //Debemos controlar que el control de cantidad se haga dentro de una transacción para controlar
        //la disponibilidad en aquellas que sean concurrentes, para que se realicen una tras otra
        //para ellos usaremos transacciones de la base de datos usan el Facade DB
        return DB::transaction(function () use ($request, $product, $buyer){
            //descontamos la cantidad del producto
            $product->quantity -= $request->quantity;
            $product->save();

            //Luego creamos la transacción
            $transaction =Transaction::create([
                'quantity' => $request->quantity,
                'buyer_id' => $buyer->id,
                'product_id' => $product->id,
            ]);

            return $this->showOne($transaction, 201);
        });

    }
}
