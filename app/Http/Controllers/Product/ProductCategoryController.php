<?php

namespace App\Http\Controllers\Product;

use App\Product;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class ProductCategoryController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product)
    {
        $categories = $product->categories;
        
        return $this->showAll($categories);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product, Category $category)
    {
        //En este método vamos a agregar una categoría a un producto, disponemos de 3 formas de hacerlo: sync, attach y syncWithoutDetaching
        //$product->categories()->sync([$category->id]): con sync sólo reemplaza las categorías existentes por la nueva categoría enviada
        //es decir no agrega a las preexistenes sino que las reemplaza, por lo que no nos sirve para est caso.
        //$product->categories()->attach([$category->id]): si bien nos agrega la categoria a la lista de categorías preexistentes, si volvemos
        //a agregar la misma categoría, attach nos permite hacerlo, lo que significaría poder asignar repetidas veces la misma categoria a un producto
        //$product->categories()->syncWithoutDetaching([$category->id]) es el que debemos utilizar, ya que agrega la nueva categoría sin eliminar las
        //anteriores
        $product->categories()->syncWithoutDetaching([$category->id]);

        return $this->showAll($product->categories);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product, Category $category)
    {
        //Nos permitirá una categoria de un producto, la categoria seguira existiendo, solo se elimina relación con ese producto
        //Se debe chequear que esa categoria exista asociada a ese producto
        if (!$product->categories()->find($category->id)) {
            return $this->errorResponse('La categoría especificada no está asociada a este producto', 404);
        }
        
        //En caso de que exista la quitamos de ese producto usando el método detach
        $product->categories()->detach([$category->id]);

        return $this->showAll($product->categories);
    }
}
