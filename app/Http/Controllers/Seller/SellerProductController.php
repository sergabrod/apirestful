<?php

namespace App\Http\Controllers\Seller;

use App\User;
use App\Seller;
use App\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SellerProductController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Seller $seller)
    {
        $products = $seller->products;
        return $this->showAll($products);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $seller)
    {
        $rules =  [
            'name' => 'required',
            'description'=> 'required',
            'quantity' => 'required|integer|min:1',
            'image' => 'required|image',
        ];

        $this->validate($request, $rules);
        
        $data = $request->all();
        $data['status'] = Product::PRODUCTO_NO_DISPONIBLE;
        $data['image'] = $request->image->store('');
        $data['seller_id'] = $seller->id;

        $product = Product::create($data);
        return $this->showOne($product, 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Seller  $seller
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Seller $seller, Product $product)
    {
        //En este caso recibimos un seller porque es alguien que ya poseia un producto
        //Las reglas de validación las ponemos para los campos que de enviarse deben cumplir
        //ciertos requisitos
        $rules =  [
            'quantity' => 'integer|min:1',
            'status' => 'in:' . Product::PRODUCTO_DISPONIBLE . ',' . Product::PRODUCTO_NO_DISPONIBLE,
            'image' => 'image',
        ];

        $this->validate($request, $rules);

         //Debemos validar si el id del vendedor es el mismo que recibimos mediante la petición
        $this->checkSeller($seller, $product);

        //Cargamos el producto con los datos recibidos por la petición, pero solo aquellos parámetros enviados que nos interesen
        $product->fill($request->only([
            'name', 
            'description', 
            'quantity',
        ]));

        //Si se solicita un cambio de estado, sólo permitiremos cambiar el estado del producto a disponible si ya tiene asociada al menos una categoría
        if ($request->has('status')) {
            $product->status = $request->status;
            
            if($product->estaDisponible() && $product->categories()->count() == 0) {
                return $this->errorResponse('Un producto activo debe tener al menos una categoría', 409);
            }
        }

        //Verificamos si el usuario envió una imágen para ser actualizada usando el método hasFile()
        //en el caso de que haya enviado, borramos la imagen anterior y luego asignamos la nueva
        if ($request->hasFile('image')) {
            Storage::delete($product->image);
            $product->image = $request->image->store('');
        }

        //Por último controlamos que se haya enviado al menos algún parámetro diferente a los valores actuales
        if ($product->isClean()) {
            return $this->errorResponse('Se debe especificar al menos un valor diferente para actualizar', 422);
        }

        $product->save();
        return $this->showOne($product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Seller  $seller
     * @return \Illuminate\Http\Response
     */
    public function destroy(Seller $seller, Product $product)
    {
        //Antes de borrar el producto, debemos verificar que sea un producto del vendedor
        //enviado a través de la API
        $this->checkSeller($seller, $product);
        Storage::delete($product->image);
        $product->delete();
        return $this->showOne($product);
    }

    protected function checkSeller(Seller $seller, Product $product)
    {
        //Debemos validar si el id del vendedor es el mismo que recibimos mediante la petición
        //No retornará una respuesta sino que disparará una exception, para evitar usar un nuevo
        //condicional que verifique el resultado de la acción
        if ($product->seller_id != $seller->id) {
            throw new HttpException(422, 'El vendedor especificado no coincide con el vendedor real del producto');
        }
    }
}
