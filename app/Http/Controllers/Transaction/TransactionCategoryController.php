<?php

namespace App\Http\Controllers\Transaction;

use App\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class TransactionCategoryController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Transaction $transaction)
    {
        //Debemos devolver la lista de todas las categorias del producto
        //de una transaction particular
        $categories = $transaction->product->categories;
        return $this->showAll($categories);
    }
}
