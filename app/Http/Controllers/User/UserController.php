<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\User;
use App\Buyer;
use App\Seller;

class UserController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = User::all();
        return $this->showAll($usuarios);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|confirmed',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $data['password'] = bcrypt($request->password);
        $data['verified'] = User::USUARIO_NO_VERIFICADO;
        $data['verification_token'] = User::generarVerificationToken();
        $data['admin'] = User::USUARIO_REGULAR;

        $usuario = User::create($data);
        return $this->showOne($usuario, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //Si usamos solo find devolverá null
        //con findOrFail devuelve un error 404 si no encuentra el usuario
        //$usuario = User::findOrFail($id);
        return $this->showOne($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //En el update los campos no son required, ya que pueden ser modificados o no
        //Sí validamos que cumplan con las demás reglas
        //Exceptuamos de la validación el id del usuario actual ya que si ingresa el mismo email
        //no dejaria actualizar los demás campos
        $rules = [
            'email' => 'email|unique:users,email,' . $user->id,
            'password' => 'min:6|confirmed',
            'admin' => 'in:' . User::USUARIO_ADMINISTRADOR . ',' . User::USUARIO_REGULAR, 
        ];

        $this->validate($request, $rules);

        if($request->has('name')) {
            $user->name = $request->name;
        }

        //Si el email es distinto al anterior generamos un nuevo verification token ya que es un nuevo email no verificado
        if($request->has('email') && $user->email != $request->email) {
            $user->verified = User::USUARIO_NO_VERIFICADO;
            $user->verification_token = User::generarVerificationToken();
            $user->email = $request->email;
        }

        if($request->has('password')) {
            $user->password = bcrypt($request->password);
        }

        if($request->has('admin')) {
            //Este cambio solo podria ser realizado por un usuario administrador
            //Por ahora solo controlaremos que sea un usuario que ya ha sido verificado
            //El codigo de error http 409 establece que hay un conflico
            if(!$user->esVerificado()) {
                return $this->errorResponse('Debe ser usuario verificado para modificar su tipo de usuario', 409);
            }
            $user->admin = $request->admin;        
        }

        //Antes de actualizar verificamos si alguno de los valores ha cambiado, de lo contrario no hacemos nada
        if(!$user->isDirty()) {
            return $this->errorResponse('Debe especificar al menos un valor diferente para actualizar', 422); 
        }

        $user->save();

        return $this->showOne($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if (Buyer::find($user->id) || Seller::find($user->id)){
            return $this->errorResponse('No puede eliminarse un recurso que está relacionado a otro', 409);
        }

        $user->delete();
        return $this->showOne($user);
    }
}
