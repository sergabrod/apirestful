<?php

 namespace App\Traits;
 
 use Illuminate\Support\Collection;
 use Illuminate\Database\Eloquent\Model;

 trait ApiResponser
 {
     /**
     * Devuelve una respuesta http de éxito
     */
     private function successResponse($data, $code)
     {
         return response()->json($data, $code);
     }

     /**
     * Devuelve una respuesta http con el mensaje de error
     * y el código del mismo
     */
     protected function errorResponse($message, $code)
     {
         return response()->json(['error' => $message, 'code' => $code], $code);
     }

     /**
     * Utilizada para devolver una colección, ej: todos los Users
     * por defecto el código de respuesta es http 200
     */
     protected function showAll(Collection $collection, $code = 200)
     {
         return $this->successResponse(['data' => $collection], $code);
     }

     /**
     * Devuelve una instancia de un modelo dado, por ej: un Seller
     * por defecto el código de respuesta en http 200
     */
     protected function showOne(Model $instance, $code = 200)
     {
         return $this->successResponse(['data' => $instance], $code);
     }
 }