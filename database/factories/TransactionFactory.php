<?php

use Faker\Generator as Faker;
use App\Seller;
use App\User;

$factory->define(App\Transaction::class, function (Faker $faker) {
    //Obtenemos todos los sellers, es decir, aquellos que poseen productos
    //con el método random obtenemos sólo una instancia de ellos (si pusieramos random(1) devolvería una colección)
    $seller = Seller::has('products')->get()->random();
    //Obtenemos un comprador, expepto el vendedor generado
    $buyer = User::all()->except($seller->id)->random();
    //Luego para el product_id obtenemos el id de cualquier producto que venda ese vendedor, aleatoriamente.

    return [
        'quantity' => $faker->numberBetween(1, 3),
        'buyer_id' => $buyer->id,
        'product_id' => $seller->products->random()->id,
    ];
});
