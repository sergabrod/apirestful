<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Category;
use App\Product;
use App\Transaction;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        
        //Primero borramos los registros de todas las tablas para evitar usar datos viejos, 
        //para evitar problemas con las FK las deshabilitamos para esta sesión
        //No válido para Postgresql
        //DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        //category_product no tiene modelo, usamos el facade DB
        DB::table('category_product')->truncate();
        Transaction::truncate();
        Category::truncate();
        Product::truncate();
        User::truncate();

        $cantidadUsuarios = 1000;
        $cantidadCategorias = 30;
        $cantidadProductos = 1000;
        $cantidadTransacciones = 1000;

        factory(User::class, $cantidadUsuarios)->create();
        factory(Category::class, $cantidadCategorias)->create();
        //Por cada producto generado asignamos de 1 a 5 categorias aleatorias
        //como el metodo all() devuelve una colección con todos los campos
        //usamos pluck para devolver solo el id de ellos, luego los atachamos a producto
        //esto hará el populate de la tabla pivote category_product
        factory(Product::class, $cantidadProductos)->create()->each(
            function($producto) {
                $categorias = Category::all()->random(mt_rand(1, 5))->pluck('id');
                $producto->categories()->attach($categorias);
            }
        );
        factory(Transaction::class, $cantidadTransacciones)->create();
    }
}
